const express = require('express');
const router = express.Router();

// # Contrôleurs
const authorController = require('../controller/author');
const bookController = require('../controller/book');
const bookInstanceController = require('../controller/bookinstance');
const genreController = require('../controller/genre');

// # Routes

// ## Auteurs
router.get('/authors', authorController.author_list);
router.get('/author/:id', authorController.author_detail);
router.put('/author', authorController.author_create);
router.post('/author/:id', authorController.author_update);
router.delete('/author/:id', authorController.author_delete);

// ## Livres
router.get('/books', bookController.book_list);
router.get('/book/:id', bookController.book_detail);
router.put('/book', bookController.book_create);
router.post('/book/:id', bookController.book_update);
router.delete('/book/:id', bookController.book_delete);

// ## Instances de livres
router.get('/bookinstances', bookInstanceController.bookinstance_list);
router.get('/bookinstance/:id', bookInstanceController.bookinstance_detail);
router.put('/bookinstance', bookInstanceController.bookinstance_create);
router.post('/bookinstance/:id', bookInstanceController.bookinstance_update);
router.delete('/bookinstance/:id', bookInstanceController.bookinstance_delete);

// ## Genres
router.get('/genres', genreController.genre_list);
router.get('/genre/:id', genreController.genre_detail);
router.put('/genre', genreController.genre_create);
router.post('/genre/:id', genreController.genre_update);
router.delete('/genre/:id', genreController.genre_delete);

module.exports = router;