const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;
const AuthorSchema = new Schema({
    first_name: {type: String, required: true, max: 100},
    last_name: {type: String, required: true, max: 100},
    date_of_birth: {type: Date},
    date_of_death: {type: Date},
}, {toJSON: {virtuals: true}});

AuthorSchema
    .virtual('name')
    .get(function () {
        return this.last_name.toUpperCase() + ' ' + this.first_name;
    });

AuthorSchema
    .virtual('lifespan')
    .get(function () {
        if (this.date_of_death && this.date_of_birth) {
            return (this.date_of_death.getYear() - this.date_of_birth.getYear()).toString();
        }
        return this.age;
    });

AuthorSchema
    .virtual('age')
    .get(function () {
        if (this.date_of_birth) {
            return (new Date().getYear() - this.date_of_birth.getYear()).toString();
        }
        return 0;
    });

AuthorSchema.virtual('date_of_birth_formatted')
    .get(function() {
        if (!this.date_of_birth) {
            return '';
        }

        return moment(this.date_of_birth).format('DD/MM/YYYY');
    });

AuthorSchema.virtual('date_of_death_formatted')
    .get(function() {
        if (!this.date_of_death) {
            return '';
        }

        return moment(this.date_of_death).format('DD/MM/YYYY');
    });

AuthorSchema
    .virtual('url')
    .get(function () {
        return '/catalog/author/' + this._id;
    });

module.exports = mongoose.model('Author', AuthorSchema);