const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;
const BookInstanceSchema = new Schema({
    book: { type: Schema.Types.ObjectId, ref: 'Book', required: true },
    imprint: {type: String, required: true},
    status: {type: String, required: true, enum: ['Disponible', 'Maintenance', 'Prêté', 'Réservé'], default: 'Maintenance'},
    due_back: {type: Date, default: Date.now}
}, {toJSON: {virtuals: true} });

BookInstanceSchema.virtual('due_back_formatted')
    .get(function() {
        if (!this.due_back) {
            return '';
        }
        return moment(this.due_back).format('DD/MM/YYYY');
    });

BookInstanceSchema.virtual('url')
    .get(function() {
        return '/catalog/bookinstance/' + this._id;
    });

module.exports = mongoose.model('BookInstance', BookInstanceSchema );