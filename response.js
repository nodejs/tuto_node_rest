const http = require('http');

// Ajout d'une fonction respond() aux objets ServerResponse, pour simplifier/uniformiser leur utilisation
http.ServerResponse.prototype.respond = function (content, status) {
    if ('undefined' === typeof status) {
        if ('number' === typeof content || !isNaN(parseInt(content))) {
            status = parseInt(content);
            content = undefined;
        } else {
            status = 200;
        }
    }
    if (status !== 200) {
        content = {
            "code": status,
            "status": http.STATUS_CODES[status],
            "message": content && content.toString() || null
        };
    }
    if ('object' !== typeof content) {
        content = {"result": content};
    }

    // On retourne du JSON
    this.type('json');
    this.status(status).send(JSON.stringify(content) + "\n")
};