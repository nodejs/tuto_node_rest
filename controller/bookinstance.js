const async = require('async');
const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');
const utils = require('../utils');
const BookInstance = require('../model/bookinstance');
const bookStatuses = ['Disponible', 'Maintenance', 'Prêté', 'Réservé'];

// Display list of all BookInstances.
exports.bookinstance_list = function (req, res) {
    try {
        BookInstance.find({}, 'book status')
            .populate('book')
            .exec(function (err, list) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                res.respond(list);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Display details for a specific BookInstance.
exports.bookinstance_detail = function (req, res) {
    try {
        const id = req.params.id;
        BookInstance.findById(id)
            .populate('book')
            .exec(function (err, list) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                res.respond(list);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Handle BookInstance create on PUT.
exports.bookinstance_create = [
    // On valide les données du body de la request
    body('book')
        .isLength({min: 1}).trim().withMessage("Un livre doit être renseigné."),
    body('imprint')
        .isLength({min: 1}).trim().withMessage("L'édition ne doit pas être vide."),
    body('due_back').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de retour doit être au format ISO8601."),
    body('status')
        .custom(function (value) {
            if (bookStatuses.indexOf(value) === -1) {
                throw new Exception("Invalid status");
            }
            return value;
        }).withMessage("Les statuts possibles sont : '" + bookStatuses.join("', '") + "'."),
    sanitizeBody('book').trim().escape(),
    sanitizeBody('imprint').trim().escape(),
    sanitizeBody('status').trim().escape(),
    sanitizeBody('due_back').toDate(),

    // On traite les données
    function (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        let item = new BookInstance({
            book: req.body.book,
            imprint: req.body.imprint,
            status: req.body.status,
            due_back: req.body.due_back,
        });

        item.save(function (err) {
            if (err) {
                res.respond("Impossible de créer l'instance du livre : " + err, 500);
                return;
            }
            res.redirect(item.url);
        });
    }
];

// Handle bookinstance update on POST.
exports.bookinstance_update = [
    // On valide les données du body de la request
    body('book').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage("Un livre doit être renseigné."),
    body('imprint').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage("L'édition ne doit pas être vide."),
    body('due_back').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de retour doit être au format ISO8601."),
    body('status').optional({checkFalsy: true})
        .custom(function (value) {
            if (bookStatuses.indexOf(value) === -1) {
                throw new Exception("Invalid status");
            }
            return value;
        }).withMessage("Les statuts possibles sont : '" + bookStatuses.join("', '") + "'."),
    sanitizeBody('book').trim().escape(),
    sanitizeBody('imprint').trim().escape(),
    sanitizeBody('status').trim().escape(),
    sanitizeBody('due_back').toDate(),

        // On traite les données
        function (req, res, next) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
                return;
            }

            async.waterfall([
                function searchItem(callback) {
                    BookInstance.findById(req.params.id)
                        .exec(function (err, item) {
                            if (err) {
                                callback({
                                    'message': "Impossible de récupérer l'instance du livre : " + err,
                                    'status': 404
                                });
                                return;
                            }

                            callback(null, item);
                        });
                },
                function updateItem(item, callback) {
                    if (req.body.book) {
                        item.book = req.body.book;
                    }
                    if (req.body.imprint) {
                        item.imprint = req.body.imprint;
                    }
                    if (req.body.status) {
                        item.status = req.body.status;
                    }
                    if (req.body.due_back) {
                        item.due_back = req.body.due_back;
                    }
                    item.save(function (err) {
                        if (err) {
                            callback({
                                'message': "Impossible de mettre à jour l'instance du livre : " + err,
                                'status': 500
                            });
                            return;
                        }

                        callback(null, item);
                    });
                }
            ], function asyncComplete(err, item) {
                if (err) {
                    res.respond(err.message, err.status);
                    return;
                }
                res.redirect(item.url);
            });
        }
    ];

// Handle BookInstance delete on DELETE.
exports.bookinstance_delete = function (req, res) {
    res.respond({message: "NOT IMPLEMENTED: BookInstance delete DELETE", method: req.method});
};