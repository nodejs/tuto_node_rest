const async = require('async');
const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');
const utils = require('../utils');
const Book = require('../model/book');
const Genre = require('../model/genre');

// Display list of all Genres.
exports.genre_list = function (req, res) {
    try {
        Genre.find({})
            .exec(function (err, list) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                res.respond(list);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Display details for a specific Genre.
exports.genre_detail = function (req, res) {
    try {
        const id = req.params.id;
        async.parallel({
                genre: function (callback) {
                    Genre.findById(id)
                        .exec(callback);
                },

                books: function (callback) {
                    Book.find({'genres': id})
                        .exec(callback);
                }
            },

            function (err, results) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                if (results.genre === null) {
                    res.respond('Genre non trouvé', 404);
                    return;
                }
                res.respond(results);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Handle Genre create on PUT.
exports.genre_create = [
    // On valide les données du body de la request
    body('name', "Le champ 'name' est requis.").isLength({min: 3, max: 100}).trim(),
    sanitizeBody('name').trim().escape(),

    // On traite les données
    function (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        let item = new Genre({
            name: req.body.name
        });

        Genre.findOne({'name': req.body.name})
            .exec(function (err, duplicateItem) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                if (duplicateItem !== null) {
                    res.respond("Un genre avec le nom existe déjà (" + duplicateItem.url + ")", 422);
                    return;
                }

                item.save(function (err) {
                    if (err) {
                        res.respond("Impossible de créer le genre : " + err, 500);
                        return;
                    }
                    res.redirect(item.url);
                });
            });
    }
];

// Handle genre update on POST.
exports.genre_update = [
    // On valide les données du body de la request
    body('name', "Le champ 'name' est requis.").isLength({min: 3, max: 100}).trim(),
    sanitizeBody('name').trim().escape(),

    // On traite les données
    function (req, res, next) {
        const id = req.params.id;

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        // Chaque fonction du tableau appelle le callback avec soit une erreur, soit des données
        // Si une erreur est passée, la fonction finale (asyncComplete()) est immédiatement appelée
        // Sinon, la fonction suivante dans le tableau est appelée, avec les données comme premier(s) argument(s)
        async.waterfall([
            function searchItem(callback) {
                Genre.findById(id)
                    .exec(function (err, item) {
                        if (err) {
                            callback({
                                'message': "Impossible de récupérer le genre : " + err,
                                'status': 404
                            });
                            return;
                        }

                        callback(null, item);
                    });
            },
            function searchOtherItemWithSameName(item, callback) {
                Genre.findOne({'name': req.body.name, '_id': {$ne: id}})
                    .exec(function (err, duplicateItem) {
                        if (err) {
                            callback({
                                'message': "Impossible de rechercher les doublons : " + err,
                                'status': 404
                            });
                            return;
                        }
                        if (duplicateItem !== null) {
                            callback({
                                'message': "Un genre avec ce nom existe déjà (" + duplicateItem.url + ")",
                                'status': 422
                            });
                            return;
                        }

                        callback(null, item);
                    });
            },
            function updateItem(item, callback) {
                item.name = req.body.name;
                item.save(function (err) {
                    if (err) {
                        callback({
                            'message': "Impossible de mettre à jour le genre : " + err,
                            'status': 500
                        });
                        return;
                    }

                    callback(null, item);
                });
            }

        ], function asyncComplete(err, item) {
            if (err) {
                res.respond(err.message, err.status);
                return;
            }
            res.redirect(item.url);
        });
    }
];

// Handle Genre delete on DELETE.
exports.genre_delete = function (req, res) {
    res.respond({message: "NOT IMPLEMENTED: Genre delete DELETE", method: req.method});
};