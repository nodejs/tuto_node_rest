const async = require('async');
const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');
const utils = require('../utils');
const Book = require('../model/book');
const BookInstance = require('../model/bookinstance');

// Display list of all Books.
exports.book_list = function (req, res) {
    try {
        Book.find({}, 'title author genres')
            .populate('author')
            .populate('genres')
            .exec(function (err, list) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                res.respond(list);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Display details for a specific Book.
exports.book_detail = function (req, res) {
    try {
        const id = req.params.id;
        async.parallel({
                book: function (callback) {
                    Book.findById(id)
                        .populate('author')
                        .populate('genres')
                        .exec(callback);
                },

                instances: function (callback) {
                    BookInstance.find({'book': id})
                        .exec(callback);
                },

                stats: function (callback) {
                    async.parallel({
                        nb_instances: function (callback) {
                            BookInstance.count({'book': id})
                                .exec(callback);
                        },
                        nb_loaned: function (callback) {
                            BookInstance.count({'book': id, 'status': 'Prêté'})
                                .exec(callback);
                        }
                    }, function (err, results) {
                        callback(null, results);
                    });
                }
            },

            function (err, results) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                if (results.book === null) {
                    res.respond('Livre non trouvé', 404);
                    return;
                }
                res.respond(results);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Handle Book create on PUT.
exports.book_create = [
    // On convertit le genre en tableau
    function (req, res, next) {
        if (req.body.genres && !(req.body.genres instanceof Array)) {
            if (typeof req.body.genres === 'undefined')
                req.body.genres = [];
            else
                req.body.genres = new Array(req.body.genres);
        }
        next();
    },

    // On valide les données du body de la request
    body('title').isLength({min: 1}).trim().withMessage('Le titre ne doit pas être vide.'),
    body('author').isLength({min: 1}).trim().withMessage("L'auteur doit être renseigné."),
    body('summary').isLength({min: 1}).trim().withMessage("Le résumé ne doit pas être vide."),
    body('isbn').isLength({min: 1}).trim().withMessage("L'ISBN doit être renseigné."),
    sanitizeBody('*').trim().escape(),

    // On traite les données
    function (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        let item = new Book({
            title: req.body.title,
            author: req.body.author,
            summary: req.body.summary,
            isbn: req.body.isbn,
            genres: req.body.genres,
        });

        Book.findOne({'isbn': req.body.isbn})
            .exec(function (err, duplicateItem) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                if (duplicateItem !== null) {
                    res.respond("Un livre avec cet ISBN existe déjà (" + duplicateItem.url + ")", 422);
                    return;
                }

                item.save(function (err) {
                    if (err) {
                        res.respond("Impossible de créer le livre : " + err, 500);
                        return;
                    }
                    res.redirect(item.url);
                });
            });
    }
];

// Handle book update on POST.
exports.book_update = [
    // On convertit le genre en tableau
    function (req, res, next) {
        if (req.body.genres && !(req.body.genres instanceof Array)) {
            if (typeof req.body.genres === 'undefined')
                req.body.genres = [];
            else
                req.body.genres = new Array(req.body.genres);
        }
        next();
    },

    // On valide les données du body de la request
    body('title').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage('Le titre ne doit pas être vide.'),
    body('author').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage("L'auteur doit être renseigné."),
    body('summary').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage("Le résumé ne doit pas être vide."),
    body('isbn').optional({checkFalsy: true})
        .isLength({min: 1}).trim().withMessage("L'ISBN doit être renseigné."),
    sanitizeBody('*').trim().escape(),

    // On traite les données
    function (req, res, next) {
        const id = req.params.id;

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        // Chaque fonction du tableau appelle le callback avec soit une erreur, soit des données
        // Si une erreur est passée, la fonction finale (asyncComplete()) est immédiatement appelée
        // Sinon, la fonction suivante dans le tableau est appelée, avec les données comme premier(s) argument(s)
        async.waterfall([
            function searchItem(callback) {
                Book.findById(id)
                    .exec(function (err, item) {
                        if (err) {
                            callback({
                                'message': "Impossible de récupérer le livre : " + err,
                                'status': 404
                            });
                            return;
                        }

                        callback(null, item);
                    });
            },
            function searchOtherItemWithSameName(item, callback) {
                if (!req.body.isbn) {
                    callback(null, item);
                    return;
                }
                Book.findOne({'isbn': req.body.isbn, '_id': {$ne: id}})
                    .exec(function (err, duplicateItem) {
                        if (err) {
                            callback({
                                'message': "Impossible de rechercher les doublons : " + err,
                                'status': 404
                            });
                            return;
                        }
                        if (duplicateItem !== null) {
                            callback({
                                'message': "Un livre avec cet ISBN existe déjà (" + duplicateItem.url + ")",
                                'status': 422
                            });
                            return;
                        }

                        callback(null, item);
                    });
            },
            function updateItem(item, callback) {
                if (req.body.title) {
                    item.title = req.body.title;
                }
                if (req.body.author) {
                    item.author = req.body.author;
                }
                if (req.body.summary) {
                    item.summary = req.body.summary;
                }
                if (req.body.isbn) {
                    item.isbn = req.body.isbn;
                }
                if (req.body.genres) {
                    item.genres = req.body.genres;
                }
                item.save(function (err) {
                    if (err) {
                        callback({
                            'message': "Impossible de mettre à jour le livre : " + err,
                            'status': 500
                        });
                        return;
                    }

                    callback(null, item);
                });
            }

        ], function asyncComplete(err, item) {
            if (err) {
                res.respond(err.message, err.status);
                return;
            }
            res.redirect(item.url);
        });
    }
];

// Handle Book delete on DELETE.
exports.book_delete = function (req, res) {
    res.respond({message: "NOT IMPLEMENTED: Book delete DELETE", method: req.method});
};