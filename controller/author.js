const async = require('async');
const moment = require('moment');
const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');
const utils = require('../utils');
const Author = require('../model/author');
const Book = require('../model/book');

exports.author_list = function (req, res) {
    try {
        Author.find({}, 'first_name last_name name')
            .sort([
                ['last_name', 'ascending'],
                ['first_name', 'ascending']
            ])
            .exec(function (err, list) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }
                res.respond(list);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Display details for a specific Author.
exports.author_detail = function (req, res) {
    try {
        const id = req.params.id;
        async.parallel({
                author: function findAuthor(callback) {
                    Author.findById(id)
                        .exec(callback);
                },

                books: function findBooksHeWrote(callback) {
                    Book.find({'author': id})
                        .populate('genres')
                        .exec(callback);
                }
            },

            function (err, results) {
                if (err) {
                    res.respond(err, 500);
                    return;
                }

                if (results.book === null) {
                    res.respond('Auteur non trouvé', 404);
                    return;
                }

                res.respond(results);
            });
    } catch (err) {
        res.respond(err, 500);
    }
};

// Handle Author create on PUT.
exports.author_create = [
    // On valide les données du body de la request
    body('first_name')
        .isLength({min: 1, max: 100}).trim().withMessage("Le prénom est requis.")
        .matches(/^[a-zA-Z\s\-]+$/, 'i').withMessage("Le prénom contient des caractères invalides."),
    body('last_name').isLength({min: 1, max: 100}).trim().withMessage("Le nom de famille est requis.")
        .matches(/^[a-zA-Z\s\-']+$/, 'i').withMessage("Le nom de famille contient des caractères invalides."),
    body('date_of_birth').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de naissance doit être au format ISO8601.")
        .custom(function (value) {
            if (!moment(value).isBefore()) {
                throw new Exception("Invalid birth date");
            }
            return value;
        }).withMessage('La date de naissance doit être dans le passé.'),
    body('date_of_death').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de mort doit être au format ISO8601.")
        .custom(function (value) {
            if (!moment(value).isBefore()) {
                throw new Exception("Invalid death date");
            }
            return value;
        }).withMessage('La date de mort doit être dans le passé.'),
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),
    sanitizeBody('date_of_death').toDate(),

    // On traite les données
    function (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        let item = new Author({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            date_of_birth: req.body.date_of_birth,
            date_of_death: req.body.date_of_death,
        });

        item.save(function (err) {
            if (err) {
                res.respond("Impossible de créer l'auteur : " + err, 500);
                return;
            }
            res.redirect(item.url);
        });
    }
];

// Handle Author update on POST.
exports.author_update = [
    // On valide les données du body de la request
    body('first_name').optional({checkFalsy: true})
        .isLength({min: 1, max: 100}).trim().withMessage("Le prénom est requis.")
        .matches(/^[a-zA-Z\s\-]+$/, 'i').withMessage("Le prénom contient des caractères invalides."),
    body('last_name').optional({checkFalsy: true})
        .isLength({min: 1, max: 100}).trim().withMessage("Le nom de famille est requis.")
        .matches(/^[a-zA-Z\s\-']+$/, 'i').withMessage("Le nom de famille contient des caractères invalides."),
    body('date_of_birth').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de naissance doit être au format ISO8601.")
        .custom(function (value) {
            if (!moment(value).isBefore()) {
                throw new Exception("Invalid birth date");
            }
            return value;
        }).withMessage('La date de naissance doit être dans le passé.'),
    body('date_of_death').optional({checkFalsy: true})
        .isISO8601().withMessage("La date de mort doit être au format ISO8601.")
        .custom(function (value) {
            if (!moment(value).isBefore()) {
                throw new Exception("Invalid death date");
            }
            return value;
        }).withMessage('La date de mort doit être dans le passé.'),
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),
    sanitizeBody('date_of_death').toDate(),

    // On traite les données
    function (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.respond("Données invalides : " + utils.stringifyErrors(errors.array()), 422);
            return;
        }

        async.waterfall([
            function searchItem(callback) {
                Author.findById(req.params.id)
                    .exec(function (err, item) {
                        if (err) {
                            callback({
                                'message': "Impossible de récupérer l'auteur : " + err,
                                'status': 404
                            });
                            return;
                        }

                        callback(null, item);
                    });
            },
            function updateItem(item, callback) {
                if (req.body.first_name) {
                    item.first_name = req.body.first_name;
                }
                if (req.body.last_name) {
                    item.last_name = req.body.last_name;
                }
                if (req.body.date_of_birth) {
                    item.date_of_birth = req.body.date_of_birth;
                }
                if (req.body.date_of_death) {
                    item.date_of_death = req.body.date_of_death;
                }
                item.save(function (err) {
                    if (err) {
                        callback({
                            'message': "Impossible de mettre à jour l'auteur : " + err,
                            'status': 500
                        });
                        return;
                    }

                    callback(null, item);
                });
            }
        ], function asyncComplete(err, item) {
            if (err) {
                res.respond(err.message, err.status);
                return;
            }
            res.redirect(item.url);
        });
    }
];

// Handle Author delete on DELETE.
exports.author_delete = function (req, res) {
    res.respond({message: "NOT IMPLEMENTED: Author delete DELETE", method: req.method});
};