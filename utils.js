module.exports = {
    stringifyErrors: function (errorList) {
        let errors = '';
        errorList.forEach(function (error) {
            errors += ' ' + error.msg;
        });

        return errors.slice(1);
    }
};