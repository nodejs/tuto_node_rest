Tuto NodeJS - API REST
======================

Ce projet est un entraitement pour créer une API REST avec NodeJS.
Il se base sur le [tutorial proposé par Mozilla](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs)

Il utilise très largement les lib suivantes :
* [express](http://expressjs.com/en/4x/api.html) : API pour simplifier la création web en NodeJS
* [mongoose](https://mongoosejs.com/docs/guide.html) : Connecteur à MongoDB
