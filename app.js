const HOSTNAME = 'localhost';
const PORT = 3000;

const DB_USER = 'db_user';
const DB_PASSWORD = 'db_password1';
const DB_SERVER = 'ds215563.mlab.com:15563';
const DB_NAME = 'tuto_node_rest';

require('./response');
const express = require('express');
const app = express();

// # Base de données MongoDB
const mongoose = require('mongoose');
mongoose.connect(`mongodb://${DB_USER}:${DB_PASSWORD}@${DB_SERVER}/${DB_NAME}`, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erreur de connexion à MongoDB :'));

// # Ajoute un parseur pour gérer les corps de requête en JSON
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// # Modèles
require('./model/author');
require('./model/book');
require('./model/bookinstance');
require('./model/genre');

// # Routeurs
app.use('/', require('./routes/index'));
app.use('/catalog', require('./routes/catalog'));

// Démarrage du serveur
app.listen(PORT, HOSTNAME, function () {
    console.log("Le serveur est démarré (http://" + HOSTNAME + ":" + PORT + ")\n");
});

